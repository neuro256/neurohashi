﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class Colors
{
    private static Color mDownColor = new Color32(255, 0, 255, 255);
    private static Color mEnterColor = new Color32(200, 128, 128, 255);
    private static Color mBridgeColor = new Color32(200, 85, 50, 255);

    public static Color downColor
    {
        get
        {
            return mDownColor;
        }
    }

    public static Color enterColor
    {
        get
        {
            return mEnterColor;
        }
    }

    public static Color bridgeColor
    {
        get
        {
            return mBridgeColor;
        }
    }
}
