﻿using System;

public interface IArchiveSerialization
{
    void Serialize(Archive ar);
}