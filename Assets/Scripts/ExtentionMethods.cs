﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class ExtentionMethods
{
    public static void RenameKey<TKey, TValue>(this IDictionary<TKey, TValue> dic,
                                  TKey fromKey, TKey toKey)
    {
        TValue value = dic[fromKey];
        dic.Remove(fromKey);
        dic[toKey] = value;
    }

    public static bool OnWindowLoad(this GameObject target)
    {
        WindowTransition transition = target.GetComponent<WindowTransition>();
        if (transition != null)
        {
            transition.OnWindowAdded();
            return true;
        }
        return false;
    }

    public static bool OnWindowRemove(this GameObject target)
    {
        WindowTransition transition = target.GetComponent<WindowTransition>();
        if (transition != null)
        {
            transition.OnWindowRemove();
            return true;
        }
        return false;
    }
}

