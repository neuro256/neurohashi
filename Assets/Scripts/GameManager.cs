﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Collections.Generic;
using SmartLocalization;
using System.Linq;

public enum SortingLayer
{
    IslandLayer = -2,
    BridgeLayer = -1,
    BackgroundLayer = 0
}

public class GameManager : MonoBehaviour
{
    public delegate void SetLevelTextHandler(int level);
    public static event SetLevelTextHandler SetLevelTextEvent;

    public static GameManager instance = null;

    [SerializeField]
    private float levelStartDelay = 0.5f;
    // private variables
    private Transform mBridgesHolder;
    private List<Island> mIslands;
    private List<Bridge> mBridgesList;
    private Island mSelectedIsland = null;
    private Island mSuggestionIsland = null;
    private string mCurrentLevelId;
    private GameObject mForegroundImage = null;
    private float mMinLength = 0.5f;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != null)
            Destroy(gameObject);
    }

    public void CreateGame(string pLevelId)
    {
        mCurrentLevelId = pLevelId;
        StopAllCoroutines();
        ClearGameBoard();
        StartCoroutine(ViewForegroundImage());
    }

    public IEnumerator ViewForegroundImage()
    {
        mForegroundImage = FindObjectOfType<GamePlay>().transform.FindChild("ForegroundImage").gameObject;
        InputManager.isTouchAvailable = false;
        mForegroundImage.SetActive(true);
        yield return StartCoroutine(CreateGameBoard());
        Invoke("HideForegroundImage", levelStartDelay);
    }

    private void HideForegroundImage()
    {
        mForegroundImage.SetActive(false);
        InputManager.isTouchAvailable = true;
    }

    public IEnumerator CreateGameBoard()
    {
        mBridgesHolder = new GameObject("Bridges").transform;
        mBridgesHolder.transform.SetParent(gameObject.transform);
        SetAspectRatio();

        mIslands = new List<Island>();
        mBridgesList = new List<Bridge>();
        CreateIslands(GetLevelData());

        yield return null;
    }

    private void SetAspectRatio()
    {
        AutoAspectRatio autoAspectRatio = FindObjectOfType<AutoAspectRatio>();
        autoAspectRatio.SetAspectRatio(GetLevelData().width);
    }

    private LevelData GetLevelData()
    {
        return LevelManager.GetInstance().GetLevelData(PlayerData.GetInstance().CurrentSetName, mCurrentLevelId);
    }

    private void CreateIslands(LevelData pLevelData)
    {
        int lGridWidth = pLevelData.width;
        int lGridHeight = pLevelData.height;
        bool isVisited = false;
        for (int x = 0; x < lGridWidth; x++)
        {
            for (int y = 0; y < lGridHeight; y++)
            {
                if (pLevelData.data[y][x] != 0)
                {
                    Vector2 position = x * Vector2.right - y * Vector2.up;
                    Island curIsland = CreateIsland(pLevelData, position);
                    VisitingRoundIsland(pLevelData, curIsland);

                    isVisited = true;
                    break;
                }
            }
            if (isVisited)
                break;
        }
    }

    private void VisitingRoundIsland(LevelData pLevelData, Island curIsland)
    {
        int x = Mathf.Abs((int)curIsland.position.x);
        int y = Mathf.Abs((int)curIsland.position.y);
        for (int i = x; i >= 0; i--)
        {
            if (pLevelData.data[y][i] != 0)
            {
                Vector2 position = i * Vector2.right - y * Vector2.up;
                if (Visiting(pLevelData, curIsland, position, new BridgeDirectionLeft()))
                    break;
            }
        }
        for (int i = x; i < pLevelData.width; i++)
        {
            if (pLevelData.data[y][i] != 0)
            {
                Vector2 position = i * Vector2.right - y * Vector2.up;
                if (Visiting(pLevelData, curIsland, position, new BridgeDirectionRight()))
                    break;
            }
        }

        for (int i = y; i >= 0; i--)
        {
            if (pLevelData.data[i][x] != 0)
            {
                Vector2 position = x * Vector2.right - i * Vector2.up;
                if (Visiting(pLevelData, curIsland, position, new BridgeDirectionUp()))
                    break;
            }
        }

        for (int i = y; i < pLevelData.height; i++)
        {
            if (pLevelData.data[i][x] != 0)
            {
                Vector2 position = x * Vector2.right - i * Vector2.up;
                if (Visiting(pLevelData, curIsland, position, new BridgeDirectionDown()))
                    break;
            }
        }
    }

    private bool Visiting(LevelData pLevelData, Island curIsland, Vector2 position, BridgeDirection pDirection)
    {
        Island lIsland = mIslands.Where(obj => obj.position == position).SingleOrDefault();
        if (lIsland != null && lIsland != curIsland) // уже существующий остров, просто добавляем к соседям текущего острова
        {
            Neighbor lNeighbor = Neighbor.Create(lIsland, pDirection);
            curIsland.AddNeighbor(lNeighbor);

            return true;
        }
        else if (lIsland == null) // это новый остров. Добавляем его в список, к соседям, и запускаем функцию 
        {
            Island newIsland = CreateIsland(pLevelData, position);
            Neighbor lNeighbor = Neighbor.Create(newIsland, pDirection);
            curIsland.AddNeighbor(lNeighbor);
            VisitingRoundIsland(pLevelData, newIsland);

            return true;
        }
        return false;
    }

    private Island CreateIsland(LevelData pLevelData, Vector2 position)
    {
        Island lIsland = Instantiate(Island.prefab, (Vector3)position + (int)SortingLayer.IslandLayer * Vector3.forward, Quaternion.identity);
        lIsland.transform.SetParent(transform);
        lIsland.position = position;
        lIsland.SetVertexCount(pLevelData.data[Mathf.Abs((int)position.y)][Mathf.Abs((int)position.x)]);
        mIslands.Add(lIsland);
        return lIsland;
    }

    public void ClearGameBoard()
    {
        if (mBridgesList != null)
        {
            foreach (var bridge in mBridgesList)
            {
                Destroy(bridge.gameObject);
            }
            mBridgesList.Clear();
            mBridgesList = null;
        }
        if (mIslands != null)
        {
            foreach (var island in mIslands)
            {
                Destroy(island.gameObject);
            }
            mIslands.Clear();
            mIslands = null;
        }
        mSelectedIsland = null;
        if (mBridgesHolder != null)
            Destroy(mBridgesHolder.gameObject);
    }

    public void ResetGameBoard()
    {
        if (mBridgesList != null)
        {
            foreach (var bridge in mBridgesList)
            {
                Destroy(bridge.gameObject);
            }
            mBridgesList.Clear();
        }
        foreach (Island island in mIslands)
        {
            if (island != null)
            {
                island.Clear();
            }
        }
    }

    public void PlayNextLevel()
    {
        LevelData lNextLevelData = LevelManager.GetInstance().GetNextLevelData(PlayerData.GetInstance().CurrentSetName, PlayerData.GetInstance().CurrentLevelId);
        if (lNextLevelData != null)
        {
            SetLevelText();
            CreateGame(lNextLevelData.id);
        }
        // Если уровней больше нет, то возвращаем на экран выбора уровней
        else
        {
            // TODO : Вывести сообщение о том, что уровней на этом наборе больше нет, поздравить игрока. 
            CanvasManager.instance.OpenSelectDifficulty(gameObject);
        }
    }

    public void PlayLevel(string pLevelId)
    {
        SetLevelText();
        CreateGame(pLevelId);
        SoundManager.instance.StartBGMusic();
    }

    private void SetLevelText()
    {
        if (SetLevelTextEvent != null)
        {
            SetLevelTextEvent(PlayerData.GetInstance().CurrentLevelIndex + 1);
        }
    }

    private void BuildBridge(Island islandA, Island islandB)
    {
        bool isSolved = false;
        // Проверяем, существует ли в этом месте мост. Если нет, то строим новый.      
        Bridge bridge = GetBridge(islandA, islandB);
        // Проверяем, можно ли провести еще один мост
        if (!islandA.isFull && !islandB.isFull)
        {
            if (bridge == null)
            {
                // Проверяем на пересечения с другими мостами
                if (!Intersection(islandA, islandB))
                {
                    AddSingleBridge(islandA, islandB);
                    isSolved = IsSolved();
                }
            }
            // Если есть одиночный, превращаем его в двойной
            else if (bridge is SingleBridge)
            {
                RemoveBridge(islandA, islandB, bridge);
                AddDoubleBridge(islandA, islandB);
                isSolved = IsSolved();
            }
            else if (bridge is DoubleBridge)
            {
                RemoveBridge(islandA, islandB, bridge);
            }
        }
        else if (bridge != null)
        {
            RemoveBridge(islandA, islandB, bridge);
        }
        if (isSolved)
        {
            SuccessLevel();
        }
    }

    private Bridge GetBridge(Island islandA, Island islandB)
    {
        return mBridgesList.Find(delegate (Bridge bridge)
        {
            return ((bridge.islandA == islandA && bridge.islandB == islandB) || (bridge.islandA == islandB && bridge.islandB == islandA));
        });
    }

    private void AddSingleBridge(Island islandA, Island islandB)
    {
        // Вычисляет расстояние между двумя островами и между ними посередине создается мост. 
        Vector2 startPos = islandA.position;
        Vector2 endPos = islandB.position;
        SingleBridge bridge = Instantiate(SingleBridge.prefab);
        bridge.transform.position = new Vector3((startPos.x + endPos.x) / 2.0f, (startPos.y + endPos.y) / 2.0f);
        bridge.transform.SetParent(mBridgesHolder);
        bridge.Add(islandA, islandB);
        // Проверка на заполнение мостами
        CheckIslands(islandA, islandB);
        mBridgesList.Add(bridge);
    }

    private void AddDoubleBridge(Island islandA, Island islandB)
    {
        // Вычисляет расстояние между двумя островами и между ними посередине создается мост. 
        Vector2 startPos = islandA.position;
        Vector2 endPos = islandB.position;
        DoubleBridge bridge = Instantiate(DoubleBridge.prefab);
        bridge.transform.position = new Vector3((startPos.x + endPos.x) / 2.0f, (startPos.y + endPos.y) / 2.0f);
        bridge.transform.SetParent(mBridgesHolder);
        bridge.Add(islandA, islandB);
        // Проверка на заполнение мостами
        CheckIslands(islandA, islandB);
        mBridgesList.Add(bridge);
    }

    private void RemoveBridge(Island pIslandA, Island pIslandB, Bridge bridge)
    {
        bridge.Remove(pIslandA, pIslandB);
        Destroy(bridge.gameObject);
        mBridgesList.Remove(bridge);
        // Проверка на заполнение мостами
        CheckIslands(pIslandA, pIslandB);
    }

    private void CheckIslands(Island islandA, Island islandB)
    {
        islandA.CheckFull();
        islandB.CheckFull();
    }

    // Проверка на завершение игры
    private bool IsSolved()
    {
        // Если оличество соединений всех островов равно количеству их вершин, то игра завершена
        foreach (Island island in mIslands)
        {
            if (island != null && !island.isFull)
            {
                return false;
            }
        }
        return true;
    }

    private void SuccessLevel()
    {
        PlayerData.GetInstance().Save();
        Invoke("ExecuteSuccessLevel", 1F);
    }

    private void ExecuteSuccessLevel()
    {
        CanvasManager.instance.OnSuccessLevel(gameObject);
    }

    /// <summary>
    /// Проверяется, пересекает ли новый отрезок (мост) любой другой ранее созданный отрезок (мост). 
    /// Взаимное расположение отрезков определяется с помощью векторных произведений.
    /// Если представить один отрезок в виде прямой, векторные произведения точек другого отрезка будут иметь противоположный знак если отрезки пересекаются. 
    /// </summary>
    /// <param name="islandA"></param>
    /// <param name="islandB"></param>
    /// <returns></returns>
    private bool Intersection(Island islandA, Island islandB)
    {
        Vector2 a1 = islandA.position;
        Vector2 a2 = islandB.position;
        foreach (Bridge bridgeB in mBridgesList)
        {
            Vector2 b1 = bridgeB.islandA.position;
            Vector2 b2 = bridgeB.islandB.position;

            float v1 = (b2.x - b1.x) * (a1.y - b1.y) - (b2.y - b1.y) * (a1.x - b1.x);
            float v2 = (b2.x - b1.x) * (a2.y - b1.y) - (b2.y - b1.y) * (a2.x - b1.x);
            float v3 = (a2.x - a1.x) * (b1.y - a1.y) - (a2.y - a1.y) * (b1.x - a1.x);
            float v4 = (a2.x - a1.x) * (b2.y - a1.y) - (a2.y - a1.y) * (b2.x - a1.x);

            if ((v1 * v2 < 0) && (v3 * v4 < 0))
            {
                Debug.Log("Intersection");
                return true;
            }
        }
        return false;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            SuccessLevel();
        }
    }

    public void OnTouchDown(GameObject pObject)
    {
        if (!InputManager.isTouchAvailable)
            return;
        Island lSelectedIsland = pObject.GetComponent<Island>();
        if (lSelectedIsland != null)
        {
            mSelectedIsland = lSelectedIsland;
            mSelectedIsland.GetComponent<SpriteRenderer>().color = Colors.downColor;
        }
    }

    public void OnTouchDrag(Vector2 pos)
    {
        if (!InputManager.isTouchAvailable)
            return;
        if (mSelectedIsland == null)
            return;

        Vector2 startPos = mSelectedIsland.position;
        Island lSuggestionIsland = mSelectedIsland.GetNeighborByDirection(startPos, pos);
        if (mSuggestionIsland != null && mSuggestionIsland != lSuggestionIsland)
            mSuggestionIsland.GetComponent<SpriteRenderer>().color = Color.white;
        mSuggestionIsland = lSuggestionIsland;

        if (mSuggestionIsland == null || mSuggestionIsland == mSelectedIsland || Vector2.Distance(startPos, pos) <= mMinLength)
            return;
        mSuggestionIsland.GetComponent<SpriteRenderer>().color = Colors.enterColor;
    }

    public void OnTouchUp(Vector2 pos)
    {
        if (!InputManager.isTouchAvailable)
            return;
        if (mSelectedIsland == null)
            return;

        if (mSuggestionIsland != null)
        {
            mSuggestionIsland.GetComponent<SpriteRenderer>().color = Color.white;
            mSuggestionIsland = null;
        }

        Vector2 startPos = mSelectedIsland.position;
        Island neighborIsland = mSelectedIsland.GetNeighborByDirection(startPos, pos);

        if (mSelectedIsland == neighborIsland)
            return;

        if (neighborIsland != null && Vector2.Distance(startPos, pos) > mMinLength)
        {
            BuildBridge(mSelectedIsland, neighborIsland);
        }
        mSelectedIsland.GetComponent<SpriteRenderer>().color = Color.white;
        mSelectedIsland = null;
    }
}
