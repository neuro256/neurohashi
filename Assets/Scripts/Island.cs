﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class Island : MonoBehaviour {

    private static Island m_Prefab = null;
    private int m_MaxVertexCount = 0;
    private int m_CurrentVertexCount = 0;
    public Vector2 position { get; set; }
    List<Neighbor> mNeighbors;
    [SerializeField]
    private Sprite m_DefaultSpr;
    [SerializeField]
    private Sprite m_CompletedSpr;

    public static Island prefab
    {
        get
        {
            if(m_Prefab == null)
            {
                m_Prefab = Resources.Load<Island>("Prefabs/Island");
            }
            return m_Prefab;
        }
    }

    void Awake()
    {
        mNeighbors = new List<Neighbor>();
    }

    void Start()
    {     
        GetComponent<SpriteRenderer>().sprite = m_DefaultSpr;
        GetComponentInChildren<TextMesh>().text = m_MaxVertexCount.ToString();
    }

    public void SetVertexCount(int vCount)
    {
        m_MaxVertexCount = vCount;
    }

    public void AddNeighbor(Neighbor neighbor)
    {
        mNeighbors.Add(neighbor);
    }

    public Island GetNeighborByDirection(Vector2 startPos, Vector2 endPos)
    {
        return BridgeDirection.GetDirection(startPos, endPos).GetNeighborByDirection(mNeighbors);
    }

    public int getMaxVertexCount
    {
        get
        {
            return m_MaxVertexCount;
        }
    }

    public int currentVertexCount
    {
        get
        {
            return m_CurrentVertexCount;
        }
        set
        {
            m_CurrentVertexCount = value;
        }
    }

    public bool isFull
    {
        get
        {
            return currentVertexCount >= getMaxVertexCount ? true : false;
        }
    }

    public void CheckFull()
    {
        if(isFull)
        {
            GetComponent<SpriteRenderer>().sprite = m_CompletedSpr;
        }
        else
        {
            GetComponent<SpriteRenderer>().sprite = m_DefaultSpr;
        }
    }

    public void Clear()
    {
        m_CurrentVertexCount = 0;
        GetComponent<SpriteRenderer>().sprite = m_DefaultSpr;
    }
}
