﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public abstract class Bridge : MonoBehaviour, IEqualityComparer<Bridge>
{
    protected int m_RouteCount;
    public Island islandA { get; private set; }
    public Island islandB { get; private set; }
    [SerializeField]
    private AudioClip buildBridgeSound1;
    [SerializeField]
    private AudioClip buildBridgeSound2;
    [SerializeField]
    private GameObject mSegmentPrefab;

    public int SetRouteCount
    {
        set
        {
            m_RouteCount = value;
        }
    }

    public void Add(Island pIslandA, Island pIslandB)
    {
        CreateSegments(pIslandA, pIslandB);
        islandA = pIslandA;
        islandB = pIslandB;
        islandA.currentVertexCount += m_RouteCount;
        islandB.currentVertexCount += m_RouteCount;
        SoundManager.instance.RandomizeSfx(buildBridgeSound1, buildBridgeSound2);
    }

    private void CreateSegments(Island pIslandA, Island pIslandB)
    {
        Vector2 startPos = pIslandA.position;
        Vector2 endPos = pIslandB.position;
        // Префаб сегмента представляет собой одинарную или двойную линию. Для того чтобы она правильно отображалась, ее нужно вращать вокруг ее центра
        // 90 градусов прибавляется потому, что изначально линия нарисовано вертикально. 
        float angle = Mathf.Atan2(endPos.y - startPos.y, endPos.x - startPos.x) * Mathf.Rad2Deg + 90.0f;
        float distance = Vector2.Distance(startPos, endPos);
        float segmentSize = mSegmentPrefab.GetComponent<SpriteRenderer>().bounds.size.y;
        // Прибавлен +1 для того, чтобы конец моста скрывался под островом
        int segmentCount = Mathf.CeilToInt(distance / segmentSize) + 1;
        // Create segments
        for (int i = 0; i < segmentCount; i++)
        {
            GameObject segment = Instantiate(mSegmentPrefab);
            segment.transform.parent = transform;
            segment.transform.position = new Vector3(((distance - segmentSize * i) * startPos.x + segmentSize * i * endPos.x) / distance,
                                                          ((distance - segmentSize * i) * startPos.y + segmentSize * i * endPos.y) / distance,
                                                           (int)SortingLayer.BridgeLayer);
            segment.transform.localRotation = Quaternion.Euler(0, 0, angle);
            segment.GetComponent<SpriteRenderer>().color = Colors.bridgeColor;
        }
    }

    public void Remove(Island pIslandA, Island pIslandB)
    {
        islandA = pIslandA;
        islandB = pIslandB;
        islandA.currentVertexCount -= m_RouteCount;
        islandB.currentVertexCount -= m_RouteCount;
    }

    public bool Equals(Bridge x, Bridge y)
    {
        if (x == null || y == null)
            return false;
        if (System.Object.ReferenceEquals(x, y))
            return true;
        if (x.GetType() != y.GetType())
            return false;
        if (x.islandA != y.islandA)
            return false;
        if (x.islandB != y.islandB)
            return false;
        if (x.m_RouteCount != y.m_RouteCount)
            return false;
        return true;
    }

    public int GetHashCode(Bridge obj)
    {
        int hashIslandA = obj.islandA.GetHashCode();
        int hashIslandB = obj.islandB.GetHashCode();
        int hashRouteCount = obj.m_RouteCount.GetHashCode();
        return hashIslandA ^ hashIslandB ^ hashRouteCount;
    }
}
