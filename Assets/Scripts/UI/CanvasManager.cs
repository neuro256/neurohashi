﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using SmartLocalization;

public class CanvasManager : MonoBehaviour {

    public static CanvasManager instance = null;
    public List<GameObject> GameScreens = new List<GameObject>();
    private GameObject LastScreen = null;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != null)
            Destroy(gameObject);
        // Set localization
        SmartCultureInfo deviceCulture = LanguageManager.Instance.GetDeviceCultureIfSupported();
        if (deviceCulture != null)
        {
            LanguageManager.Instance.ChangeLanguage(deviceCulture);
            Debug.Log("Change language with smart culture info");
        }
        else
        {
            LanguageManager.Instance.ChangeLanguage("en");
            Debug.Log("Change language default en");
        }
    }

    void Start()
    {
        Application.targetFrameRate = 60; 
        SpawnScreen("IntroMenu");
        PlayerData.GetInstance().Load();
    }

    private void SpawnScreen(string name)
    {
        GameObject thisScreen = null;
        thisScreen = GameScreens.Where(obj => obj.name == name).SingleOrDefault();
        thisScreen.Init();
        thisScreen.OnWindowLoad();
        thisScreen.SetActive(true);
        LastScreen = thisScreen;
    }

    public void OnSuccessLevel(GameObject currentScreen)
    {
        SpawnScreen("WinMenu");
    }

    public void OpenIntro(GameObject currentScreen)
    {
        currentScreen.OnWindowRemove();
        currentScreen.SetActive(false);
        SpawnScreen("IntroMenu");
    }

    public void StartTheGame(GameObject currentScreen)
    {
        currentScreen.OnWindowRemove();
        currentScreen.SetActive(false);
        SpawnScreen("GamePlay");
        GameManager.instance.PlayLevel(PlayerData.GetInstance().CurrentLevelId);
    }

    public void PlayNextLevel(GameObject currentScreen)
    {
        currentScreen.OnWindowRemove();
        currentScreen.SetActive(false);
        SpawnScreen("GamePlay");
        GameManager.instance.PlayNextLevel();
    }

    public void ReplayCurrentLevel(GameObject currentScreen)
    {
        currentScreen.OnWindowRemove();
        currentScreen.SetActive(false);
        SpawnScreen("GamePlay");
        GameManager.instance.PlayLevel(PlayerData.GetInstance().CurrentLevelId);
    }

    public void OpenHelpWindow(GameObject currentScreen)
    {
        currentScreen.OnWindowRemove();
        currentScreen.SetActive(false);
        SpawnScreen("HelpWindow");
    }

    public void OpenSelectDifficulty(GameObject currentScreen)
    {
        currentScreen.OnWindowRemove();
        currentScreen.SetActive(false);
        SpawnScreen("SelectDifficulty");
    }

    public void OpenSelectLevel(GameObject currentScreen, string levelsSetName)
    {
        currentScreen.OnWindowRemove();
        currentScreen.SetActive(false);
        SpawnScreen("SelectLevel");
        LastScreen.GetComponent<SelectLevel>().CreateLevels(levelsSetName);
    }

    public void CancelQuitConfirm(GameObject currentScreen)
    {
        currentScreen.OnWindowRemove();
        LastScreen = GameScreens.Where(obj => obj.name == "IntroMenu").SingleOrDefault();
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape) && InputManager.isTouchAvailable)
        {
            OnBackButtonPressed();
        }
    }

    private void OnBackButtonPressed()
    {
        if(LastScreen.name == "IntroMenu")
        {
            SpawnScreen("QuitConfirm");
        }
        else if(LastScreen.name == "HelpWindow")
        {
            LastScreen.OnWindowRemove();
            LastScreen.SetActive(false);
            SpawnScreen("IntroMenu");
        }
        else if(LastScreen.name == "SelectDifficulty")
        {
            LastScreen.OnWindowRemove();
            LastScreen.SetActive(false);
            SpawnScreen("IntroMenu");
        }
        else if(LastScreen.name == "SelectLevel")
        {
            LastScreen.OnWindowRemove();
            LastScreen.SetActive(false);
            SpawnScreen("SelectDifficulty");
        }
        else if(LastScreen.name == "GamePlay")
        {
            LastScreen.OnWindowRemove();
            LastScreen.SetActive(false);
            SpawnScreen("SelectLevel");
            GameManager.instance.ClearGameBoard();
            SoundManager.instance.StopBGMusic();
            LastScreen.GetComponent<SelectLevel>().CreateLevels(PlayerData.GetInstance().CurrentSetName);
        }
        else if(LastScreen.name == "WinMenu")
        {
            PlayNextLevel(LastScreen);
        }
    }
}
