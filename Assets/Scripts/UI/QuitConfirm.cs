﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using SmartLocalization;

public class QuitConfirm : MonoBehaviour {

    private static QuitConfirm m_Prefab = null;

    public Text quitConfirmText;

    public static QuitConfirm prefab
    {
        get
        {
            if(m_Prefab == null)
            {
                m_Prefab = Resources.Load<QuitConfirm>("Prefabs/QuitConfirm");
            }
            return m_Prefab;
        }
    }

    void Start()
    {
        quitConfirmText.text = LanguageManager.Instance.GetTextValue("GUI:QuitConfirmText");
    }

    public void OnCancelButtonPressed()
    {
        CanvasManager.instance.CancelQuitConfirm(gameObject);
    }

    public void OnQuitButtonPressed()
    {
        Application.Quit();
    }
}
