﻿using SmartLocalization;
using System;
using UnityEngine;
using UnityEngine.UI;

public class HelpWindow : MonoBehaviour
{
    private static HelpWindow m_Prefab = null;

    [SerializeField]
    private Button btnOk;
    [SerializeField]
    private Text textPanel;
    [SerializeField]
    private Text titleText;

    public static HelpWindow prefab
    {
        get
        {
            if(m_Prefab == null)
            {
                m_Prefab = Resources.Load<HelpWindow>("Prefabs/HelpWindow");
            }
            return m_Prefab;
        }
    }

    void Start()
    {
        btnOk.GetComponentInChildren<Text>().text = LanguageManager.Instance.GetTextValue("GUI:Ok");
        textPanel.text = LanguageManager.Instance.GetTextValue("TEXT:Rules");
        titleText.text = LanguageManager.Instance.GetTextValue("GUI:RulesTitle");
    }

    public void OnOkButtonPressed()
    {
        CanvasManager.instance.OpenIntro(gameObject);
    }
}
