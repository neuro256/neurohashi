﻿using SmartLocalization;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelectLevel : MonoBehaviour
{
    private static SelectLevel m_Prefab = null;

    [SerializeField]
    private Transform buttonListTransform;
    [SerializeField]
    private Text titleText;
    [SerializeField]
    private int columns = 4;
    private List<SelectLevelButton> mSelectLevelButtonList = null;

    public static SelectLevel prefab
    {
        get
        {
            if(m_Prefab == null)
            {
                m_Prefab = Resources.Load<SelectLevel>("Prefabs/SelectLevel");
            }
            return m_Prefab;
        }
    }

    void OnDisable()
    {
        ButtonListClear();
    }

    public void CreateLevels(string pLevelsSetName)
    {
        SetTitle(LanguageManager.Instance.GetTextValue("GUI:" + pLevelsSetName));
        RectTransform myRect = buttonListTransform.GetComponent<RectTransform>();
        float buttonSize = myRect.rect.width / (float)columns;
        GridLayoutGroup grid = buttonListTransform.GetComponent<GridLayoutGroup>();
        grid.cellSize = new Vector2(buttonSize, buttonSize) - grid.spacing;
        mSelectLevelButtonList = new List<SelectLevelButton>();
        List<LevelData> lLevelsData = LevelManager.GetInstance().GetLevelsSetByName(pLevelsSetName);
        if(lLevelsData != null)
        {
            for(int i = 0; i < lLevelsData.Count; i++)
            {
                SelectLevelButton lSelectLevelButton = Instantiate(SelectLevelButton.prefab, buttonListTransform, false) as SelectLevelButton;
                lSelectLevelButton.Id = i;
                lSelectLevelButton.LevelId = lLevelsData[i].id;
                lSelectLevelButton.CheckCompleted();
                lSelectLevelButton.SetText((i + 1).ToString());
                mSelectLevelButtonList.Add(lSelectLevelButton);
            }
        }
    }

    private void SetTitle(string pValue)
    {
        titleText.GetComponent<Text>().text = pValue;
    }

    public void ButtonListClear()
    {
        if (mSelectLevelButtonList != null)
        {
            foreach (var button in mSelectLevelButtonList)
            {
                Destroy(button.gameObject);
            }
            mSelectLevelButtonList.Clear();
        }
    }
}
