﻿using SmartLocalization;
using System;
using UnityEngine;
using UnityEngine.UI;

public class SelectDifficulty : MonoBehaviour
{
    private static SelectDifficulty m_Prefab = null;

    [SerializeField]
    private Button btnEasy;
    [SerializeField]
    private Button btnMedium;
    [SerializeField]
    private Button btnHard;

    public static SelectDifficulty prefab
    {
        get
        {
            if(m_Prefab == null)
            {
                m_Prefab = Resources.Load<SelectDifficulty>("Prefabs/SelectDifficulty");
            }
            return m_Prefab;
        }
    }

    void Start()
    {
        btnEasy.transform.FindChild("Title").GetComponent<Text>().text = LanguageManager.Instance.GetTextValue("GUI:Easy");
        btnMedium.transform.FindChild("Title").GetComponent<Text>().text = LanguageManager.Instance.GetTextValue("GUI:Medium");
        btnHard.transform.FindChild("Title").GetComponent<Text>().text = LanguageManager.Instance.GetTextValue("GUI:Hard");      
    }

    void OnEnable()
    {
        btnEasy.transform.FindChild("Stats").GetComponent<Text>().text = LevelManager.GetInstance().GetCompletedLevelsCountOnSet("Easy") + "/" + LevelManager.GetInstance().GetLevelsSetSize("Easy");
        btnMedium.transform.FindChild("Stats").GetComponent<Text>().text = LevelManager.GetInstance().GetCompletedLevelsCountOnSet("Medium") + "/" + LevelManager.GetInstance().GetLevelsSetSize("Medium");
        btnHard.transform.FindChild("Stats").GetComponent<Text>().text = LevelManager.GetInstance().GetCompletedLevelsCountOnSet("Hard") + "/" + LevelManager.GetInstance().GetLevelsSetSize("Hard");
    }

    public void OnSelectEasyButtonPressed()
    {
        CanvasManager.instance.OpenSelectLevel(gameObject, "Easy");
    }

    public void OnSelectMediumButtonPressed()
    {
        CanvasManager.instance.OpenSelectLevel(gameObject, "Medium");
    }

    public void OnSelectHardButtonPressed()
    {
        CanvasManager.instance.OpenSelectLevel(gameObject, "Hard");
    }
}
