﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class SelectLevelButton : MonoBehaviour
{
    private static SelectLevelButton m_Prefab;
    private int mId;
    private string mLevelId;

    [SerializeField]
    private Sprite defaultSpr;
    [SerializeField]
    private Sprite completedSpr;

    public static SelectLevelButton prefab
    {
        get
        {
            if(m_Prefab == null)
            {
                m_Prefab = Resources.Load<SelectLevelButton>("Prefabs/LevelButton");
            }
            return m_Prefab;
        }
    }

    public int Id
    {
        get
        {
            return mId;
        }
        set
        {
            mId = value;
        }
    }

    public string LevelId
    {
        get
        {
            return mLevelId;
        }
        set
        {
            mLevelId = value;
        }
    }

    public void OnLevelSelectButtonPressed()
    {
        PlayerData.GetInstance().CurrentLevelId = mLevelId;
        PlayerData.GetInstance().CurrentLevelIndex = mId;
        CanvasManager.instance.StartTheGame(FindObjectOfType<SelectLevel>().gameObject);
    }

    public void CheckCompleted()
    {
        if (PlayerData.GetInstance().IsCompleted(LevelId))
        {
            gameObject.GetComponent<Button>().image.overrideSprite = completedSpr;
        }
        else
        {
            gameObject.GetComponent<Button>().image.overrideSprite = defaultSpr;
        }
    }

    public void SetText(string pText)
    {
        GetComponentInChildren<Text>().text = pText;
    }
}
