﻿using SmartLocalization;
using System;
using UnityEngine;
using UnityEngine.UI;

public class IntroMenu : MonoBehaviour
{
    private static IntroMenu m_Prefab = null;
    [SerializeField]
    private Button btnPlay;
    [SerializeField]
    private Button btnHelp;
    [SerializeField]
    private Text titleText;

    public static IntroMenu prefab
    {
        get
        {
            if(m_Prefab == null)
            {
                m_Prefab = Resources.Load<IntroMenu>("Prefabs/IntroMenu");
            }
            return m_Prefab;
        }
    }

    void Start()
    {
        btnPlay.GetComponentInChildren<Text>().text = LanguageManager.Instance.GetTextValue("GUI:Play");
        btnHelp.GetComponentInChildren<Text>().text = LanguageManager.Instance.GetTextValue("GUI:Help");
        titleText.text = LanguageManager.Instance.GetTextValue("GUI:AppName");
    }

    public void OnPlayButtonPressed()
    {
        CanvasManager.instance.OpenSelectDifficulty(gameObject);
    }

    public void OnHelpButtonPressed()
    {
        CanvasManager.instance.OpenHelpWindow(gameObject);
    }
}
