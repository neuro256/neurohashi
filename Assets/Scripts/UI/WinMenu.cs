﻿using SmartLocalization;
using UnityEngine;
using UnityEngine.UI;

public class WinMenu : MonoBehaviour
{
    private static WinMenu m_Prefab = null;

    [SerializeField]
    private Text winMenuTitle;

    public static WinMenu prefab
    {
        get
        {
            if(m_Prefab == null)
            {
                m_Prefab = Resources.Load<WinMenu>("Prefabs/WinMenu");
            }
            return m_Prefab;
        }
    }

    void Start()
    {
        winMenuTitle.text = LanguageManager.Instance.GetTextValue("GUI:WinTitle");
    }
    public void OnPlayNextLevelButtonPressed()
    {
        CanvasManager.instance.PlayNextLevel(gameObject);
    }

    public void OnReplayCurrentLevel()
    {
        CanvasManager.instance.ReplayCurrentLevel(gameObject);
    }
}
