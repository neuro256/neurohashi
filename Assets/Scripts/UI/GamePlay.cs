﻿using SmartLocalization;
using System;
using UnityEngine;
using UnityEngine.UI;

public class GamePlay : MonoBehaviour
{
    private static GamePlay m_Prefab = null;

    [SerializeField]
    private Text mLevelText;
    // Buttons
    [SerializeField]
    private Button mButtonSettings;
    [SerializeField]
    private Button mButtonOpenIntro;
    [SerializeField]
    private Button mButtonReset;
    [SerializeField]
    private Button mButtonSound;
    [SerializeField]
    private Button mButtonMusic;
    private bool isButtonsContainerActive = false;
    private GameObject mScreenFader = null;

    public static GamePlay prefab
    {
        get
        {
            if(m_Prefab == null)
            {
                m_Prefab = Resources.Load<GamePlay>("Prefabs/GamePlay");
            }
            return m_Prefab;
        }
    }

    void Awake()
    {
        SetButtons();
    }

    void OnEnable()
    {
        GameManager.SetLevelTextEvent += SetLevelText;
    }

    void OnDisable()
    {
        GameManager.SetLevelTextEvent -= SetLevelText;
    }

    private void SetButtons()
    {
        mButtonSettings.onClick.AddListener(OnClickSettings);
        mButtonOpenIntro.onClick.AddListener(OnClickOpenIntro);
        mButtonReset.onClick.AddListener(OnClickReset);

        int soundOn = PlayerPrefs.GetInt("SOUND_ON", 1);
        int musicOn = PlayerPrefs.GetInt("MUSIC_ON", 1);
        if(soundOn == 0)
        {
            mButtonSound.transform.GetChild(0).gameObject.SetActive(false);
            mButtonSound.transform.GetChild(1).gameObject.SetActive(true);
        }
        else
        {
            mButtonSound.transform.GetChild(0).gameObject.SetActive(true);
            mButtonSound.transform.GetChild(1).gameObject.SetActive(false);
        }
        if(musicOn == 0)
        {
            mButtonMusic.transform.GetChild(0).gameObject.SetActive(false);
            mButtonMusic.transform.GetChild(1).gameObject.SetActive(true);
        }
        else
        {
            mButtonMusic.transform.GetChild(0).gameObject.SetActive(true);
            mButtonMusic.transform.GetChild(1).gameObject.SetActive(false);
        }

        mButtonSound.onClick.AddListener(OnClickSound);
        mButtonMusic.onClick.AddListener(OnClickMusic);

        mScreenFader = FindObjectOfType<GamePlay>().transform.FindChild("ScreenFader").gameObject;

        ResetButtons();
    }

    public void ResetButtons()
    {
        mButtonOpenIntro.transform.gameObject.SetActive(false);
        mButtonReset.transform.gameObject.SetActive(false);
        mButtonSound.transform.gameObject.SetActive(false);
        mButtonMusic.transform.gameObject.SetActive(false);
        isButtonsContainerActive = false;
        InputManager.isTouchAvailable = true;
        mScreenFader.SetActive(false);
    }

    private void OnClickSettings()
    {
        if (isButtonsContainerActive)
        {
            mButtonOpenIntro.transform.gameObject.SetActive(false);
            mButtonReset.transform.gameObject.SetActive(false);
            mButtonSound.transform.gameObject.SetActive(false);
            mButtonMusic.transform.gameObject.SetActive(false);
            InputManager.isTouchAvailable = true;
            mScreenFader.SetActive(false);
        }
        else
        {
            mButtonOpenIntro.transform.gameObject.SetActive(true);
            mButtonReset.transform.gameObject.SetActive(true);
            mButtonSound.transform.gameObject.SetActive(true);
            mButtonMusic.transform.gameObject.SetActive(true);
            InputManager.isTouchAvailable = false;
            mScreenFader.SetActive(true);
        }
        isButtonsContainerActive = !isButtonsContainerActive;
    }

    public void OnClickOpenIntro()
    {
        CanvasManager.instance.OpenIntro(gameObject);
        GameManager.instance.ClearGameBoard();
        SoundManager.instance.StopBGMusic();
        ResetButtons();
    }

    public void OnClickReset()
    {
        GameManager.instance.ResetGameBoard();
        OnClickSettings();
    }

    private void OnClickSound()
    {
        int soundOn = PlayerPrefs.GetInt("SOUND_ON", 1);
        if (soundOn == 1)
        {
            SoundManager.instance.SoundActive = false;
            PlayerPrefs.SetInt("SOUND_ON", 0);
            mButtonSound.transform.GetChild(0).gameObject.SetActive(false);
            mButtonSound.transform.GetChild(1).gameObject.SetActive(true);
        }
        else
        {
            SoundManager.instance.SoundActive = true;
            PlayerPrefs.SetInt("SOUND_ON", 1);
            mButtonSound.transform.GetChild(0).gameObject.SetActive(true);
            mButtonSound.transform.GetChild(1).gameObject.SetActive(false);
        }
        PlayerPrefs.Save();
    }

    private void OnClickMusic()
    {
        int musicOn = PlayerPrefs.GetInt("MUSIC_ON", 1);
        if (musicOn == 1)
        {
            SoundManager.instance.MusicActive = false;
            PlayerPrefs.SetInt("MUSIC_ON", 0);
            mButtonMusic.transform.GetChild(0).gameObject.SetActive(false);
            mButtonMusic.transform.GetChild(1).gameObject.SetActive(true);
        }
        else
        {
            SoundManager.instance.MusicActive = true;
            PlayerPrefs.SetInt("MUSIC_ON", 1);
            mButtonMusic.transform.GetChild(0).gameObject.SetActive(true);
            mButtonMusic.transform.GetChild(1).gameObject.SetActive(false);
        }
        PlayerPrefs.Save();
    }

    public void OnBackButtonPressed()
    {
        CanvasManager.instance.OpenSelectLevel(gameObject, PlayerData.GetInstance().CurrentSetName);
        GameManager.instance.ClearGameBoard();
        SoundManager.instance.StopBGMusic();
        ResetButtons();
    }

    public void SetLevelText(int level)
    {
        mLevelText.text = LanguageManager.Instance.GetTextValue("GUI:Level") + " " + level.ToString() + 
            "(" + PlayerData.GetInstance().CurrentLevelId.ToString() + ")"; // debug view id
    }
}
