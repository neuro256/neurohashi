﻿using UnityEngine;

public class DoubleBridge : Bridge
{
    public static DoubleBridge m_Prefab = null;

    public static DoubleBridge prefab
    {
        get
        {
            if (m_Prefab == null)
            {
                m_Prefab = Resources.Load<DoubleBridge>("Prefabs/DoubleBridge");
            }
            return m_Prefab;
        }
    }

    public DoubleBridge()
    {
        SetRouteCount = 2;
    }
}