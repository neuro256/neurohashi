﻿using UnityEngine;

public class SingleBridge : Bridge
{
    public static SingleBridge m_Prefab = null;

    public static SingleBridge prefab
    {
        get
        {
            if (m_Prefab == null)
            {
                m_Prefab = Resources.Load<SingleBridge>("Prefabs/Bridge");
            }
            return m_Prefab;
        }
    }
    public SingleBridge()
    {
        SetRouteCount = 1;
    }
}