﻿public struct Neighbor
{
    public Island neighbor;
    private BridgeDirection direction;

    public static Neighbor Create(Island pNeighbor, BridgeDirection pDirection)
    {
        return new Neighbor(pNeighbor, pDirection);
    }

    private Neighbor(Island pNeighbor, BridgeDirection pDirection)
    {
        neighbor = pNeighbor;
        direction = pDirection;
    }

    public BridgeDirection getDirection()
    {
        return direction;
    }
}