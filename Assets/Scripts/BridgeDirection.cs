﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class BridgeDirection
{
    public static BridgeDirection GetDirection(Vector2 startPos, Vector2 endPos)
    {
        Vector2 destinationPos = endPos - startPos;

        if (Vector2.Angle(Vector2.left, destinationPos) < 45.0f)
        {
            return new BridgeDirectionLeft();
        }
        if (Vector2.Angle(Vector2.right, destinationPos) < 45.0f)
        {
            return new BridgeDirectionRight();
        }
        if (Vector2.Angle(Vector2.down, destinationPos) < 45.0f)
        {
            return new BridgeDirectionDown();
        }
        if (Vector2.Angle(Vector2.up, destinationPos) < 45.0f)
        {
            return new BridgeDirectionUp();
        }
        return null;
    }

    public abstract Island GetNeighborByDirection(List<Neighbor> p_Neighbors);
}

public class BridgeDirectionUp : BridgeDirection
{
    public override Island GetNeighborByDirection(List<Neighbor> p_Neighbors)
    {
        return p_Neighbors.Where(obj => obj.getDirection() is BridgeDirectionUp).SingleOrDefault().neighbor;
    }
}

public class BridgeDirectionDown : BridgeDirection
{
    public override Island GetNeighborByDirection(List<Neighbor> p_Neighbors)
    {
        return p_Neighbors.Where(obj => obj.getDirection() is BridgeDirectionDown).SingleOrDefault().neighbor;
    }
}

public class BridgeDirectionLeft : BridgeDirection
{
    public override Island GetNeighborByDirection(List<Neighbor> p_Neighbors)
    {
        return p_Neighbors.Where(obj => obj.getDirection() is BridgeDirectionLeft).SingleOrDefault().neighbor;
    }
}

public class BridgeDirectionRight : BridgeDirection
{
    public override Island GetNeighborByDirection(List<Neighbor> p_Neighbors)
    {
        return p_Neighbors.Where(obj => obj.getDirection() is BridgeDirectionRight).SingleOrDefault().neighbor;
    }
}