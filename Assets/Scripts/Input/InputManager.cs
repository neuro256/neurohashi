﻿using System;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static InputManager instance;
    public static bool isTouchAvailable = true;

    Collider2D current = null;
    Collider2D drag = null;

    void Awake()
    {
        if(instance == null)
        {
            instance = this;
            return;
        }
        Destroy(gameObject);
    }

    void Update()
    {
        #region MOBILE
        if (Application.isMobilePlatform)
        {
            Vector2 position = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            RaycastHit2D hit = Physics2D.Raycast(position, Vector2.zero);
            Touch touch = Input.touches[0];
            if (hit) 
            {
                if (current == null)
                {
                    if(touch.phase == TouchPhase.Began)
                    {
                        current = hit.collider;
                        //current.SendMessage("OnTouchDown", SendMessageOptions.DontRequireReceiver);
                        drag = current;
                        GameManager.instance.OnTouchDown(current.gameObject);
                    }
                    else if(touch.phase == TouchPhase.Moved)
                    {
                        current = hit.collider;
                        //current.SendMessage("OnTouchEnter", SendMessageOptions.DontRequireReceiver);
                    }
                }
                else
                {
                    //current.SendMessage("OnTouchOver", SendMessageOptions.DontRequireReceiver);
                    if (touch.phase == TouchPhase.Ended)
                    {
                        //current.SendMessage("OnTouchUp", SendMessageOptions.DontRequireReceiver);
                        GameManager.instance.OnTouchUp(position);
                        if (current == drag)
                        {
                            //current.SendMessage("OnTouchUpAsButton", SendMessageOptions.DontRequireReceiver);
                            drag = null;
                        }
                        current = null;
                    }
                }
            }
            else
            {
                if (current != null)
                {
                    //current.SendMessage("OnTouchExit", SendMessageOptions.DontRequireReceiver);
                    current = null;
                }
                if(touch.phase == TouchPhase.Began)
                {
                    drag = null;
                }
            }
            if (drag != null)
            {
                //drag.SendMessage("OnTouchDrag", SendMessageOptions.DontRequireReceiver);
                if (touch.phase == TouchPhase.Moved)
                {
                    GameManager.instance.OnTouchDrag(position);
                }
                if(touch.phase == TouchPhase.Ended)
                {
                    GameManager.instance.OnTouchUp(position);
                }
            }
        }
        #endregion
        #region EDITOR
        else
        {
            Vector2 position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D hit = Physics2D.Raycast(position, Vector2.zero);
            if (hit)
            {
                if (current == null)
                {
                    current = hit.collider;
                    //current.SendMessage("OnTouchEnter", SendMessageOptions.DontRequireReceiver);
                }
                else
                {
                    //current.SendMessage("OnTouchOver", SendMessageOptions.DontRequireReceiver);
                    if (Input.GetMouseButtonDown(0))
                    {
                        //current.SendMessage("OnTouchDown", SendMessageOptions.DontRequireReceiver);
                        drag = current;
                        GameManager.instance.OnTouchDown(current.gameObject);
                    }
                    if (Input.GetMouseButtonUp(0))
                    {
                        GameManager.instance.OnTouchUp(position);
                        //current.SendMessage("OnTouchUp", SendMessageOptions.DontRequireReceiver);
                        if (current == drag)
                        {
                            //current.SendMessage("OnTouchUpAsButton", SendMessageOptions.DontRequireReceiver);
                            drag = null;
                        }
                    }
                }
            }
            else
            {
                if (current != null)
                {
                    //current.SendMessage("OnTouchExit", SendMessageOptions.DontRequireReceiver);
                    current = null;
                }
                if (Input.GetMouseButtonDown(0))
                {
                    drag = null;
                }
            }
            if (drag != null)
            {
                GameManager.instance.OnTouchDrag(position);
                if (Input.GetMouseButtonUp(0))
                {
                    GameManager.instance.OnTouchUp(position);
                }
            }
        }
        #endregion
        // Debug Keys
        if (!Application.isMobilePlatform)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                PlayerData.GetInstance().ResetData();
                Debug.Log("Reset player data");
            }
        }
    }
}