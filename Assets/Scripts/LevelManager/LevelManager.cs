﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class LevelManager : Singleton<LevelManager>
{
    private string mPathFile = "Data/LevelData";
    private Dictionary<string, List<LevelData>> mLevelsData = new Dictionary<string, List<LevelData>>();

    public LevelManager()
    {
        Parse();
    }

    public List<LevelData> GetLevelsSetByName(string pLevelsSetName)
    {
        if (mLevelsData.ContainsKey(pLevelsSetName))
        {
            PlayerData.GetInstance().CurrentSetName = pLevelsSetName;
            return mLevelsData[pLevelsSetName];
        }
        return null;
    }

    public int GetLevelsSetSize(string pLevelsSetName)
    {
        if (mLevelsData.ContainsKey(pLevelsSetName))
        {
            return mLevelsData[pLevelsSetName].Count;
        }
        return 0;
    }

    public int GetCompletedLevelsCountOnSet(string pLevelsSetName)
    {
        int lCompletedLevelsCount = 0;
        if (mLevelsData.ContainsKey(pLevelsSetName))
        {
            foreach(var level in mLevelsData[pLevelsSetName])
            {
                if(PlayerData.GetInstance().IsCompleted(level.id))
                {
                    lCompletedLevelsCount++;
                }
            }
        }
        return lCompletedLevelsCount;
    }

    public LevelData GetLevelData(string pSetName, string pLevelId)
    {
        return mLevelsData[pSetName].Where(obj => obj.id == pLevelId).SingleOrDefault();
    }

    public LevelData GetNextLevelData(string pSetName, string pLevelId)
    {
        LevelData lLevelData = mLevelsData[pSetName].Where(obj => obj.id == pLevelId).SingleOrDefault();
        int currentLevelIndex = mLevelsData[pSetName].IndexOf(lLevelData);
        int nextLevelIndex = currentLevelIndex;
        LevelData nextLevelData = null;
        for(int i = 0; i < mLevelsData[pSetName].Count; i++)
        {
            nextLevelIndex++;
            if(nextLevelIndex >= mLevelsData[pSetName].Count)
            {
                nextLevelIndex = 0;
            }
            nextLevelData = mLevelsData[pSetName][nextLevelIndex];
            if (nextLevelData != null && !PlayerData.GetInstance().IsCompleted(nextLevelData.id))
            {
                PlayerData.GetInstance().CurrentLevelIndex = nextLevelIndex;
                PlayerData.GetInstance().CurrentLevelId = nextLevelData.id;
                break;
            }
            nextLevelData = null;
        }
        return nextLevelData;
    }

    private void Parse()
    {
        string lDecodeString = "";
        try
        {
            TextAsset lTextAsset = (TextAsset)Resources.Load(mPathFile);
            lDecodeString = lTextAsset.ToString();
        }
        catch
        {
            Debug.LogError("CANNOT READ FOR : " + GetType());
        }

        JSONObject l_JSONObject = new JSONObject(lDecodeString);
        for (int i = 0; i < l_JSONObject.Count; i++)
        {
            string lSetId = l_JSONObject.keys[i];
            List<LevelData> lLevelsSetData = new List<LevelData>();
            for (int j = 0; j < l_JSONObject[i].Count; j++)
            {
                string lId = l_JSONObject[i][j]["Id"].str;
                int lDifficulty = (int)l_JSONObject[i][j]["Difficulty"].n;
                int lWidth = (int)l_JSONObject[i][j]["Width"].n;
                int lHeight = (int)l_JSONObject[i][j]["Height"].n;
                List<List<int>> lData = new List<List<int>>();
                for (int k = 0; k < l_JSONObject[i][j]["LevelData"].Count; k++)
                {
                    List<int> lRowData = new List<int>();
                    for (int l = 0; l < l_JSONObject[i][j]["LevelData"][k].Count; l++)
                    {
                        lRowData.Add((int)l_JSONObject[i][j]["LevelData"][k][l].n);
                    }
                    lData.Add(lRowData);
                }
                LevelData lLevelData = new LevelData(lId, lDifficulty, lWidth, lHeight, lData);
                lLevelsSetData.Add(lLevelData);
            }
            mLevelsData.Add(lSetId, lLevelsSetData);
        }
    }
}
