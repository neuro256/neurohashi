﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public class LevelData
{
    public string id;
    public int difficulty;
    public int width;
    public int height;
    public List<List<int>> data;

    public LevelData(string pId, int pDifficulty, int pWidth, int pHeight, List<List<int>> pData)
    {
        id = pId;
        difficulty = pDifficulty;
        width = pWidth;
        height = pHeight;
        data = pData;
    }
}
