﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public struct StoredData
{
    public string levelId;
}

public struct CurrentLevelData
{
    public string setName;
    public int levelIndex;
    public string levelId;
}

public class PlayerData : Singleton<PlayerData>, IArchiveSerialization
{
    private Dictionary<string, StoredData> storedData;
    private CurrentLevelData currentLevelData;

    #region СВОЙСТВА

    public string CurrentSetName
    {
        get
        {
            return currentLevelData.setName;
        }
        set
        {
            currentLevelData.setName = value;
        }
    }

    public string CurrentLevelId
    {
        get
        {
            return currentLevelData.levelId;
        }
        set
        {
            currentLevelData.levelId = value;
        }
    }

    public int CurrentLevelIndex
    {
        get
        {
            return currentLevelData.levelIndex;
        }
        set
        {
            currentLevelData.levelIndex = value;
        }
    }

    #endregion

    public PlayerData()
    {
        storedData = new Dictionary<string, StoredData>();
    }

    private void AddCurrentLevel()
    {
        StoredData lStoredData;
        lStoredData.levelId = currentLevelData.levelId;

        if (storedData.ContainsKey(currentLevelData.levelId)) 
        {
            storedData[currentLevelData.levelId] = lStoredData;
        }
        else
        {
            storedData.Add(currentLevelData.levelId, lStoredData);
        }
    }

    public bool IsCompleted(string pId)
    {
        return storedData.ContainsKey(pId);
    }

    public void ResetData()
    {
        if(storedData != null)
        {
            storedData.Clear();
            Save();
        }
    }

    public void Serialize(Archive ar)
    {
        if(ar.IsStoring())
        {
            ar.Write(storedData.Count);
            foreach(var data in storedData)
            {
                ar.Write(data.Key);
                ar.Write(data.Value.levelId);
            }
        }
        else
        {
            storedData = new Dictionary<string, StoredData>();
            int count = 0;
            ar.Read(out count);
            StoredData lStoredData;
            string id = string.Empty;
            for(int i = 0; i < count; i++)
            {
                ar.Read(out id);
                ar.Read(out lStoredData.levelId);
                storedData.Add(id, lStoredData);
            }
        }
    }

    public void Save()
    {
        AddCurrentLevel();
        FileStream file = File.Create(Application.persistentDataPath + "/PlayerData.nhd");
        Debug.Log("(Save) Path: " + Application.persistentDataPath + "/PlayerData.nhd");
        var archive = new Archive(file, ArchiveOp.store);
        Serialize(archive);
        file.Close();
    }

    public void Load()
    {
        if(File.Exists(Application.persistentDataPath + "/PlayerData.nhd"))
        {
            FileStream file = File.Open(Application.persistentDataPath + "/PlayerData.nhd", FileMode.Open);
            Debug.Log("(Load) Path: " + Application.persistentDataPath + "/PlayerData.nhd");
            var arhcive = new Archive(file, ArchiveOp.load);
            Serialize(arhcive);
            file.Close();
        }
    }
}
